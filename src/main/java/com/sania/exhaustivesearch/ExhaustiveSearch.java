/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sania.exhaustivesearch;

/**
 *
 * @author ASUS
 */
public class ExhaustiveSearch {
    public int maxValue(int[] prices) {
        int minPrice = Integer.MAX_VALUE,maxPrice = 0;
        for (int i = 0; i < prices.length; i++) {
            if (minPrice > prices[i]) {
                minPrice = prices[i];
            }else {
                maxPrice = Math.max(maxPrice, prices[i] - minPrice);
            }
        }
        
        return maxPrice;
    }
    
}

